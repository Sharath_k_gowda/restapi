
package com.intime.tec.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.intime.tec.model.Person;

@RestController
@CrossOrigin(origins = { "http://127.0.0.1:5500" })
public class HomeController {
	private static Map<String, Person> map;

	static {
		map = new HashMap<String, Person>();
		map.put("Tester", new Person("Sharath", "Software Engineer"));
		map.put("developer", new Person("Rahul", "Software Engineer"));
		System.out.println();

	}

	public HomeController() {
		System.out.println(this.getClass().getSimpleName() + "\t" + "obj Created");

	}

	@GetMapping(value = "/person")
	// @CrossOrigin(origins = "http://localhost:4200")
	public Person person() {
		return map.get("developer");
	}

	@GetMapping("/person1")
	// @CrossOrigin(origins = "http://localhost:4200")
	public Person greet() {
		return map.get("Tester");
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> receive(@RequestBody Person person) {
		map.put("BA", person);
		System.out.println(person);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/delete/{name}")
	public ResponseEntity<?> delete(@RequestBody Person person, @PathVariable String name) {
		System.out.println(person);
		Person removed = map.remove(name);
		System.out.println("deleted   " + removed);
		Set<Entry<String, Person>> entrySet = map.entrySet();
		for (Entry<String, Person> entry : entrySet) {
			System.out.println(entry.getValue());
		}
		return new ResponseEntity<>(HttpStatus.OK);

	}

	@GetMapping(value = "/all")
	public ResponseEntity<Person> all() {
		Person value;
		Set<Entry<String, Person>> entrySet = map.entrySet();
		for (Entry<String, Person> entry : entrySet) {
			value = entry.getValue();
			System.out.println(value);
		}

		return new ResponseEntity<Person>(HttpStatus.OK);

	}

}
