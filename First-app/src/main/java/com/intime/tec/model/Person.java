package com.intime.tec.model;



public class Person extends Object{

	private String name;
	private String designation;

	public Person() {
		super();
		System.out.println(this.getClass().getSimpleName() + "\t object created");
	}

	public Person(String name, String designation) {
		super();
		System.out.println(this.getClass().getSimpleName() + "\t object created");
		this.name = name;
		this.designation = designation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", designation=" + designation + "]";
	}

}
